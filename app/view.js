var app = app || {};

app.IndexView = Backbone.View.extend({
    el: ".app-container",
    initialize: function () {
        var _this = this,
            location = new app.Location(),
            jedi = new app.Jedi({
                id: "3616"
            }),
            locationView = new app.LocationView({
                model: location
            }),
            listView = this.listView = new app.ListView({
                collection: app.jedisList
            });

        function addToCollection(model) {
            if (app.jedisList.length < 5) {
                app.jedisList.push(model);
            }
        }

        location.on("change", function locationOnChange(model) {
            locationView.render();
            _this.showLocationMatch(model);
        });

        app.jedisList.on("update", function jedisListOnUpdate(collection) {
            console.log("update");
            listView.render();
            
            _this.setScrollAble();

        });
         app.jedisList.on("add", function(){

         });

        this.ws(location);

        app.jedisList.fillDown();
        _this.setScrollAble();
    },
    events: {
        "click .css-button-up:not(.css-button-disabled)": "scrollUp",
        "click .css-button-down:not(.css-button-disabled)": "scrollDown"
    },
    showLocationMatch: function(model){
      var _this = this;
      var match = _.filter(app.jedisList.models, function (jedi) {
          return jedi.attributes.homeworld.id == model.id;
      });
      if (match.length) {
          _this.requestsAborted = _this.abortRequests();
          _this.listView.clearHighlightSlot();
          _this.disableButtons();
          _.each(match, function (model) {
              _this.listView.highlightSlot(model.attributes.id);
          });
      } else {
          _this.enableButtons();
          _this.setScrollAble();
          _this.listView.clearHighlightSlot();
          if(_this.requestsAborted){
            _this.requestsAborted = _this.resumeRequests();
          }
      }
    },
    setScrollAble: function () {
        if (app.jedisList.length == 0){
            this.disableButtons();
            return;
          }

        if (app.jedisList.first().attributes.master.id === null) {
           this.disableButtons("up");
        } else {
           this.enableButtons("up");
        }

        if (app.jedisList.model.length < 5) {
           this.disableButtons("down");
        } else {
           this.enableButtons("down");
        }

        if (app.jedisList.last().attributes.apprentice.id === null) {
          this.disableButtons("down");
         } else {
            this.enableButtons("down");
        }
    },
    disableButtons: function (button) {
        var _this = this;
        switch (button) {
            case 'up':
                _this.$el.find(".css-button-up").addClass("css-button-disabled");
                break
            case 'down':
                _this.$el.find(".css-button-down").addClass("css-button-disabled");
                break
            default:
                _this.$el.find(".css-button-down, .css-button-up").addClass("css-button-disabled");
        }
    },
    enableButtons: function (button) {
        var _this = this;
        switch (button) {
            case 'up':
                _this.$el.find(".css-button-up").removeClass("css-button-disabled");
                break
            case 'down':
                _this.$el.find(".css-button-down").removeClass("css-button-disabled");
                break
            default:
                _this.$el.find(".css-button-disabled").removeClass("css-button-disabled");
        }
    },
    ws: function (model) {
        var _this = this;
        var url = 'ws://jedi.smartjs.academy';
        var ws = new WebSocket(url);

        ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            model.set(data);
        };
        ws.onclose = function (e) {
            console.info("WebSocket was closed, reconnection... ", new Date());
            _this.ws(model);
        }
    },
    scrollUp: function (e) {
        app.jedisList.remove(app.jedisList.at(4 - this.listView.offset), {
            silent: true
        });
        app.jedisList.remove(app.jedisList.at(3 - this.listView.offset), {
            silent: true
        });

        this.listView.offset == 4 ? this.listView.offset = 4 : this.listView.offset += 2;

        this.listView.render();

        this.disableButtons("up");

        app.jedisList.requests = app.jedisList.fillUp().requests.filter(function(item){
            if(item.direction == "down") {
                //(item.xhr && item.xhr.abort) ?
                item.xhr.abort();
                return false;
            }
            return true;
        });
        //app.jedisList.menageQueue("up");

    },
    scrollDown: function (e) {
        app.jedisList.remove(app.jedisList.first(), {
            silent: true
        });
        app.jedisList.remove(app.jedisList.first(), {
            silent: true
        });

        this.listView.offset = 0;

        this.listView.render();

        this.disableButtons("down");

        app.jedisList.requests = app.jedisList.fillDown().requests.filter(function(item){
            if(item.direction == "up") {
                //(item.xhr && item.xhr.abort) ?
                item.xhr.abort();
                return false;
            }
            return true;
        });
        //app.jedisList.menageQueue("down");
    },
    abortRequests: function(){
      if (app.jedisList.requests) {
        app.jedisList.requests.map(function(item){
          if(item.xhr && item.xhr.abort){
            item.xhr.abort();
          }
        });
        return true;
      }
      return false;
    },
    resumeRequests: function(){
      app.jedisList.requests.map(function(item){
        if(item.direction == "up") {
          app.jedisList.fillUp();
        } if(item.direction == "down") {
          app.jedisList.fillDown();
        }
      });
      return false;
    },
    render: function () {},
});

app.LocationView = Backbone.View.extend({
    el: "#location",
    template: _.template($("#locationView").html()),
    initialize: function () {},
    events: {},
    render: function () {
        this.$el.html(this.template(this.model.attributes));
        return this;
    },
});

app.ListView = Backbone.View.extend({
    el: "#jedis",
    template: _.template($("#jedisListItem").html()),
    initialize: function () {
        this.offset = 0;
    },
    events: {},
    highlightSlot: function (id) {
        this.$el.find('#' + id).addClass("css-slot-highlight");
    },
    removeHighlightSlot: function (id) {
        this.$el.find('li').removeClass("css-slot-highlight");
    },
    clearHighlightSlot: function () {
        this.$el.find('.css-slot-highlight').removeClass("css-slot-highlight");
    },
    render: function () {
        var _this = this;

        var slots = _this.$el.find('li')

        _.each(slots, function (slot, index) {
            if (index < _this.offset) {
                slot.id = "";
                slot.innerHTML = "";
            } else {
                var jedi = _this.collection.at(index - _this.offset);

                if (!jedi) {
                    slot.id = "";
                    slot.innerHTML = "";
                    return;
                }
                slot.id = jedi.id;
                slot.innerHTML = _this.template(jedi.attributes);
            }

        });
        /*this.collection.each(function(jedi, index){
            console.log(_this.offset)

                var slot = slots.get(index + _this.offset);
                if(index < _this.offset){
                    slots.get(index).innerHTML = "";
                }
                slot.id = jedi.attributes.id;
                slot.innerHTML =_this.template(jedi.attributes);

        })*/
        if (this.offset)
            this.offset--;

        return this;
    },
});
