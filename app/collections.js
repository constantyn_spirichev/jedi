var app = app || {};

app.jedisListCollection = Backbone.Collection.extend({
    model: app.Jedi,

    initialize: function () {
        this.initId = 3616;
        this.limit = 5;
        this.requests = [];
        this.queue = [];

    },
    menageQueue: function(action){
        var _this = this;
        if(_this.requests.length){
            var req = _this.requests[_this.requests.length-1];
            if(action == req.direction){
                switch(action){
                    case "up":
                        req.xhr.then(function(){_this.fillUp()});
                    case "down":
                        req.xhr.then(function(){_this.fillDown()});
                }
                return _this;
            } else {
                req.xhr.abort();
            }
        }
        switch(action){
            case "up":
                _this.fillUp();
            case "down":
               _this.fillDown();
        }
        return _this;
    },
    fillUp: function () {
        var _this = this,
            id = _this.models.length ? _this.first().attributes.master.id : _this.initId;
       console.log(_this.requests.length);

        if (id !== null) {
            var jedi = new _this.model({
                    id: id
                }),
                xhr = jedi.fetch({
                    success: function(model){
                        _this.requests = _this.requests.filter((item) => item.id != id);
                        _this.unshift(model);
                        if (_this.length < _this.limit) {
                            _this.fillUp();
                        }
                    }
                }),
                request = {xhr:xhr, direction: "up", id:id};

                _this.requests.push(request);

        }
        return _this;
    },
    fillDown: function () {
        var _this = this,
            id = _this.models.length ? _this.last().attributes.apprentice.id : _this.initId;

         console.log(_this.requests.length);

        if (id !== null) {
            var jedi = new _this.model({
                    id: id
                }),
                xhr = jedi.fetch({success: function(model){
                        _this.requests = _this.requests.filter(function(item){return item.id != id});
                        _this.push(model);
                        if (_this.length < _this.limit) {
                            _this.fillDown();
                        }
                    }
                }),
                request = {xhr:xhr, direction: "down", id:id};

            _this.requests.push(request);
        }
        return _this;
    }
});

app.jedisList = new app.jedisListCollection();
